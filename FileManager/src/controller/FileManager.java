package controller;

import java.io.File;
import java.util.ArrayList;

/**
 * Class FileManager for interacting with files.
 * @author Valentin Ros Duart
 */
public class FileManager {
    
    // Class file object
    private File root;
    
    /**
     * Constructor method. Creates a file if it is a dir.
     * @param uri URI String of the file(folder) to operate with.
     */
    public FileManager(String uri) {
        try {
            File root = new File(uri);
            if(root.isDirectory()) {
                this.root = root;
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * Method for retrieving files from a file(folder) and storing them in an ArrayLists which contains
     * ArrayLists with some attributes for each file within that folder.
     * @return files ArrayList that contains ArrayLists with some file attributes (name, extension, size and type)
     */
    public ArrayList getFiles() {
        try {
            if(root.isDirectory()) {
                // Create an ArrayList for later return
                ArrayList files = new ArrayList();
                // Store files root folder files in an array
                File[] childs = root.listFiles();
                // For each file in the array, create an ArrayList and store (name, extension, size and type)
                for(File child : childs) {
                    ArrayList file = new ArrayList();
                    String name = "";
                    String ext = "";
                    String type = "";
                    Long size = child.length();
                    // If the file is a dir get the full name and set type to dir
                    if(child.isDirectory()) {
                        name = child.getName();
                        type = "Dir";
                    // If the file is not a dir get the extension, the name without the extension and set type to file
                    } else {
                        // If the file contains a period and is not a hidden file (starts with a period)
                        if(child.getName().contains(".") && child.getName().lastIndexOf(".") != 0) {
                            int pos = child.getName().lastIndexOf(".");
                            ext = child.getName().substring(pos);
                            name = child.getName().substring(0, pos);
                        } else {
                            name = child.getName();
                        }
                        type = "File";
                    }
                    // Add attributes to the ArrayList file (will later be row indexes in the JTable Model)
                    file.add(name);
                    file.add(ext);
                    file.add(size);
                    file.add(type);
                    // Add the ArrayList file to ArrayList files (will later be rows in the JTable Model)
                    files.add(file);
                }
                return files;
            } else {
                return null;
            }
        } catch(SecurityException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
}
package model;

import java.util.ArrayList;

/**
 * Table model.
 * @author Valentin Ros Duart.
 */
public class Table {
    
    String name;
    ArrayList<Column> columns;
    
    public Table(String name, ArrayList<Column> columns) {
        this.name = name;
        this.columns = columns;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Column> getColumns() {
        return columns;
    }

    public void setColumns(ArrayList<Column> columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        return "Table{" + "name=" + name + ", columns=" + columns + '}';
    }
    
}

package filesixteenbits;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class copy, mimics the MS-DOS COPY command.
 * @author Valentin Ros Duart
 */
public class Copy {
    
    public static FileReader fr;
    public static FileWriter fw;
    
    /**
     * Main
     * @param args command line args
     */
    public static void main(String[] args) {
        if(copy(args) && args.length > 0) {
            System.out.println("File " + args[0] + " copied to " + args[1]);
        }
    }
    
    /**
     * Copies file content.
     * @param args command line args
     * @return true if succeeds false if it doesn't
     */
    private static boolean copy(String[] args) {
        String read = "";
        File fileRead, fileWrite;
        try {
            if(args.length == 0) {
                fileRead = new File("src\\filesixteenbits\\Copy.java");
                fileWrite = new File("src\\filesixteenbits\\Copy.cp");
            } else {
                String pathRead = args[0];
                String pathWrite = args[1];
                fileRead = new File(pathRead);
                fileWrite = new File(pathWrite);
            }
            fr = new FileReader(fileRead);
            int asci;
            asci = fr.read();
            while(asci != -1) {
                read += (char) asci;
                asci = fr.read();
            }
            fr.close();
            fw = new FileWriter(fileWrite);
            fw.write(read);
            fw.close();
            return true;
        } catch(FileNotFoundException | NullPointerException | SecurityException ex) {
            System.out.println("Error: " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            return false;
        }
    }
    
}

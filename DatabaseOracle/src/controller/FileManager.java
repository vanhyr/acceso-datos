package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Valentin Ros Duart
 */
public class FileManager {
    
    private File file;
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    
    public FileManager(String uri) {
        file = new File(uri);
    }
    
    public ArrayList<String> readLines() {
        String line;
        ArrayList<String> lines = new ArrayList<>();
        try {
            if (file.exists()) {
                try {
                    fileReader = new FileReader(file);
                    bufferedReader= new BufferedReader(fileReader);
                    while ((line = bufferedReader.readLine()) != null) {
                        lines.add(line);
                    }
                    bufferedReader.close();
                    fileReader.close();
                } catch (FileNotFoundException ex) {
                    System.out.println(ex.getMessage());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SecurityException ex) {
            System.out.println(ex.getMessage());
        }
        return lines;
    }
    
}

package util;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;

/**
 * Class for displaying digital date/time.
 * @author Valentin Ros Duart.
 */
public class DigitalClock {

    public DigitalClock(JLabel label, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Date date = getDate();
                    String dateString = simpleDateFormat.format(date);
                    label.setText(dateString);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread t = new Thread(runnable);
        t.start();
    }

    public static java.util.Date getDate() {
        java.util.Date date = new java.util.Date();
        return date;
    }
}

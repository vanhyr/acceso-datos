package controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Class Employee for interacting with employees
 * @author Valentin Ros Duart
 */
public class Employee implements Serializable {

    private int id;
    private String name;
    private Double bonus;
    private LocalDate admission;
    private String department;
    private int age;

    /**
     * Constructor method. Assign values to the attributes
     * @param id employee's id
     * @param name employee's name
     * @param bonus employee's bonification
     * @param admission employee's admission Date
     * @param department employee's department
     * @param age employee's age
     */
    public Employee(int id, String name, Double bonus, LocalDate admission, String department, int age) {
        if(id > 0 && !name.isEmpty() && bonus > 0 && !department.isEmpty() && age > 0) {
            this.id = id;
            this.name = name;
            this.bonus = bonus;
            this.admission = admission;
            this.department = department;
            this.age = age;
        }
    }

    /**
     * 
     * @return employee's id
     */
    public int getId() {
        return id;
    }
    
    /**
     * 
     * @param id employee's id
     */
    public void setId(int id) {
        try {
            if(id > 0) {
                this.id = id;           
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * 
     * @return employee's name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name empoyee's name 
     */
    public void setName(String name) {
        try {
            if(!name.isEmpty()) {
                this.name = name;            
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * 
     * @return employee's bonus
     */
    public Double getBonus() {
        return bonus;
    }

    /**
     * 
     * @param bonus empolyee's bonus
     */
    public void setBonus(Double bonus) {
        try {
            if(bonus > 0) {
                this.bonus = bonus;            
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * 
     * @return employees admission date
     */
    public LocalDate getAdmission() {
        return admission;
    }

    /**
     * 
     * @param admission employee's admission date
     */
    public void setAdmission(LocalDate admission) {
        try {
            this.admission = admission;
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * 
     * @return employee's department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * 
     * @param department employee's department
     */
    public void setDepartment(String department) {
        try {
            if(!department.isEmpty()) {
                this.department = department;
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * 
     * @return employee's age
     */
    public int getAge() {
        return age;
    }

    /**
     * 
     * @param age employee's age
     */
    public void setAge(int age) {
        try {
            if(age > 0) {
                this.age = age;            
            }
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * 
     * @return employee's object to String
     */
    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", bonus=" + bonus + ", admission=" + admission + ", department=" + department + ", age=" + age + '}';
    }
    
    /**
     * 
     * @return employee's ArrayList with it's attributes
     */
    public ArrayList toArrayList() {
        ArrayList employeeArrayList = new ArrayList();
        employeeArrayList.add(id);
        employeeArrayList.add(name);
        employeeArrayList.add(bonus);
        employeeArrayList.add(admission);
        employeeArrayList.add(department);
        employeeArrayList.add(age);
        return employeeArrayList;
    }
    
}